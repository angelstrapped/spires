extends Node

export(NodePath) var seedInputPath
export(NodePath) var randomiseButtonPath
export(NodePath) var generateButtonPath

onready var seedInput: TextEdit = get_node(seedInputPath)
onready var randomiseButton: Button = get_node(randomiseButtonPath)
onready var generateButton: Button = get_node(generateButtonPath)

var towerSeed: int = 0

func _ready():
	# Not sure what is the correct way to utilise the return value of connect().
	# Some sort of error handling, no doubt. But I don't know what I would do,
	# other than crash, if connect() threw an error...
	var _connected
	_connected = seedInput.connect("text_changed", self, "_on_SeedInput_text_changed")
	_connected = randomiseButton.connect("pressed", self, "_on_RandomiseButton_pressed")
	_connected = generateButton.connect("pressed", self, "_on_GenerateButton_pressed")

func _on_SeedInput_text_changed():
	pass

func _on_GenerateButton_pressed():
	# This is going to be quick and dirty for now.
	# Force the contents of the seed input box to be a number that Godot is able
	# to store in an int.

	# There's a small bug whereby a 19-digit decimal number can be smaller or
	# larger than the 64 bit signed int maximum value, so very high numbers with
	# 19 or more digits will all become 9223372036854775807.

	self.towerSeed = int(seedInput.text.left(19))
	seedInput.text = str(self.towerSeed)

	print(self.towerSeed)

func _on_RandomiseButton_pressed():
	# Populate a full 64 bit int (minus the sign bit, I think?) of random stuff.
	
	# First generate a random number. randi() returns a 32 bit int.
	# It will look something like 00001011 (zeroes in the upper half).
	# Then shift those bits into the upper half of the 64 bit int.
	# It will look like 10110000 (now the good numbers are in the upper half).

	# Finally, generate another random number, and OR it with the shifted int.
	# 1011000 OR
	# 00001101 =
	# 10111101

	self.towerSeed = (randi() << 31) | randi()
	seedInput.text = str(self.towerSeed)
